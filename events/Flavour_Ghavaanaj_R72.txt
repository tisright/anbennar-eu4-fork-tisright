
namespace = ghavaanaj

country_event = { #The White Guild
	id = ghavaanaj.1
	title = ghavaanaj.1.t
	desc = ghavaanaj.1.desc
	picture = GOOD_WITH_MONARCH_eventPicture
	
	is_triggered_only = yes
	
	option = {
		#! Add privilege to grant the guild monopolies over ivory and porcelain
	}
	option = {
		#! Add privilege to gain direct control over the trade with the guild being displeased
	}
}



#############################
#####   Herd Secrets   ######
#############################

country_event = { #Menu
	id = ghavaanaj.1000
	title = ghavaanaj.1000.t
	desc = ghavaanaj.1000.desc
	picture = GOOD_WITH_MONARCH_eventPicture
	
	is_triggered_only = yes
	
	immediate = {
		set_country_flag = ghavaanaj_herd_unlock_menu_open
	}
	
	option = { #Nevermind
		name = flavor_castanor_go_back
		highlight = yes
		clr_country_flag = ghavaanaj_herd_unlock_menu_open
	}
	option = { #White Herd
		name = ghavaanaj.1000.a
		trigger = { NOT = { has_country_flag = ghavaanaj_white_herd_unlocked_flag } }
		goto = 4469
		
		if = {
			limit = {
				tribal_allegiance = 60
				4469 = {
					owned_by = ROOT
					is_core = ROOT
					culture = ghavaanaj
					is_prosperous = yes
				}
			}
			4469 = {
				remove_province_modifier = ghavaanaj_white_herd_locked
				add_permanent_province_modifier = { name = ghavaanaj_white_herd_unlocked duration = -1 }
			}
			hidden_effect = { set_country_flag = ghavaanaj_white_herd_unlocked_flag }
		}
		else = {
			custom_tooltip = ghavaanaj_herd_unlock_trigger_tooltip
		}
	}
	option = { #Red Herd
		name = ghavaanaj.1000.b
		trigger = { NOT = { has_country_flag = ghavaanaj_red_herd_unlocked_flag } }
		goto = 4479
		
		if = {
			limit = {
				tribal_allegiance = 60
				4479 = {
					owned_by = ROOT
					is_core = ROOT
					culture = ghavaanaj
					is_prosperous = yes
				}
			}
			4479 = {
				remove_province_modifier = ghavaanaj_red_herd_locked
				add_permanent_province_modifier = { name = ghavaanaj_red_herd_unlocked duration = -1 }
			}
			hidden_effect = { set_country_flag = ghavaanaj_red_herd_unlocked_flag }
		}
		else = {
			custom_tooltip = ghavaanaj_herd_unlock_trigger_tooltip
		}
	}
	option = { #Purple Herd
		name = ghavaanaj.1000.c
		trigger = { NOT = { has_country_flag = ghavaanaj_purple_herd_unlocked_flag } }
		goto = 4508
		
		if = {
			limit = {
				tribal_allegiance = 60
				4508 = {
					owned_by = ROOT
					is_core = ROOT
					culture = ghavaanaj
					is_prosperous = yes
				}
			}
			4508 = {
				remove_province_modifier = ghavaanaj_purple_herd_locked
				add_permanent_province_modifier = { name = ghavaanaj_purple_herd_unlocked duration = -1 }
			}
			hidden_effect = { set_country_flag = ghavaanaj_purple_herd_unlocked_flag }
		}
		else = {
			custom_tooltip = ghavaanaj_herd_unlock_trigger_tooltip
		}
	}
	option = { #Black Herd
		name = ghavaanaj.1000.d
		trigger = { NOT = { has_country_flag = ghavaanaj_black_herd_unlocked_flag } }
		goto = 4486
		
		if = {
			limit = {
				tribal_allegiance = 60
				4486 = {
					owned_by = ROOT
					is_core = ROOT
					culture = ghavaanaj
					is_prosperous = yes
				}
			}
			4486 = {
				remove_province_modifier = ghavaanaj_black_herd_locked
				add_permanent_province_modifier = { name = ghavaanaj_black_herd_unlocked duration = -1 }
			}
			hidden_effect = { set_country_flag = ghavaanaj_black_herd_unlocked_flag }
		}
		else = {
			custom_tooltip = ghavaanaj_herd_unlock_trigger_tooltip
		}
	}
	option = { #Palace Herd
		name = ghavaanaj.1000.e
		trigger = { NOT = { has_country_flag = ghavaanaj_palace_herd_unlocked_flag } }
		goto = 4477
		
		if = {
			limit = {
				tribal_allegiance = 60
				4477 = {
					owned_by = ROOT
					is_core = ROOT
					culture = ghavaanaj
					is_prosperous = yes
				}
			}
			4477 = {
				remove_province_modifier = ghavaanaj_palace_herd_locked
				add_permanent_province_modifier = { name = ghavaanaj_palace_herd_unlocked duration = -1 }
			}
			hidden_effect = { set_country_flag = ghavaanaj_palace_herd_unlocked_flag }
		}
		else = {
			custom_tooltip = ghavaanaj_herd_unlock_trigger_tooltip
		}
	}
	option = { #Castle Herd
		name = ghavaanaj.1000.f
		trigger = { NOT = { has_country_flag = ghavaanaj_castle_herd_unlocked_flag } }
		goto = 4483
		
		if = {
			limit = {
				tribal_allegiance = 60
				4483 = {
					owned_by = ROOT
					is_core = ROOT
					culture = ghavaanaj
					is_prosperous = yes
				}
			}
			4483 = {
				remove_province_modifier = ghavaanaj_castle_herd_locked
				add_permanent_province_modifier = { name = ghavaanaj_castle_herd_unlocked duration = -1 }
			}
			hidden_effect = { set_country_flag = ghavaanaj_castle_herd_unlocked_flag }
		}
		else = {
			custom_tooltip = ghavaanaj_herd_unlock_trigger_tooltip
		}
	}
	
	after = {
		if = {
			limit = { has_country_flag = ghavaanaj_herd_unlock_menu_open }
			country_event = { id = ghavaanaj.1001 }
		}
	}
}

country_event = { #Menu 2
	id = ghavaanaj.1001
	title = ghavaanaj.1000.t
	desc = ghavaanaj.1000.desc
	picture = GOOD_WITH_MONARCH_eventPicture
	
	is_triggered_only = yes
	
	immediate = {
		set_country_flag = ghavaanaj_herd_unlock_menu_open
	}
	
	option = { #Nevermind
		name = flavor_castanor_go_back
		highlight = yes
		clr_country_flag = ghavaanaj_herd_unlock_menu_open
	}
	option = { #White Herd
		name = ghavaanaj.1000.a
		trigger = { NOT = { has_country_flag = ghavaanaj_white_herd_unlocked_flag } }
		goto = 4469
		
		if = {
			limit = {
				tribal_allegiance = 60
				4469 = {
					owned_by = ROOT
					is_core = ROOT
					culture = ghavaanaj
					is_prosperous = yes
				}
			}
			4469 = {
				remove_province_modifier = ghavaanaj_white_herd_locked
				add_permanent_province_modifier = { name = ghavaanaj_white_herd_unlocked duration = -1 }
			}
			hidden_effect = { set_country_flag = ghavaanaj_white_herd_unlocked_flag }
		}
		else = {
			custom_tooltip = ghavaanaj_herd_unlock_trigger_tooltip
		}
	}
	option = { #Red Herd
		name = ghavaanaj.1000.b
		trigger = { NOT = { has_country_flag = ghavaanaj_red_herd_unlocked_flag } }
		goto = 4479
		
		if = {
			limit = {
				tribal_allegiance = 60
				4479 = {
					owned_by = ROOT
					is_core = ROOT
					culture = ghavaanaj
					is_prosperous = yes
				}
			}
			4479 = {
				remove_province_modifier = ghavaanaj_red_herd_locked
				add_permanent_province_modifier = { name = ghavaanaj_red_herd_unlocked duration = -1 }
			}
			hidden_effect = { set_country_flag = ghavaanaj_red_herd_unlocked_flag }
		}
		else = {
			custom_tooltip = ghavaanaj_herd_unlock_trigger_tooltip
		}
	}
	option = { #Purple Herd
		name = ghavaanaj.1000.c
		trigger = { NOT = { has_country_flag = ghavaanaj_purple_herd_unlocked_flag } }
		goto = 4508
		
		if = {
			limit = {
				tribal_allegiance = 60
				4508 = {
					owned_by = ROOT
					is_core = ROOT
					culture = ghavaanaj
					is_prosperous = yes
				}
			}
			4508 = {
				remove_province_modifier = ghavaanaj_purple_herd_locked
				add_permanent_province_modifier = { name = ghavaanaj_purple_herd_unlocked duration = -1 }
			}
			hidden_effect = { set_country_flag = ghavaanaj_purple_herd_unlocked_flag }
		}
		else = {
			custom_tooltip = ghavaanaj_herd_unlock_trigger_tooltip
		}
	}
	option = { #Black Herd
		name = ghavaanaj.1000.d
		trigger = { NOT = { has_country_flag = ghavaanaj_black_herd_unlocked_flag } }
		goto = 4486
		
		if = {
			limit = {
				tribal_allegiance = 60
				4486 = {
					owned_by = ROOT
					is_core = ROOT
					culture = ghavaanaj
					is_prosperous = yes
				}
			}
			4486 = {
				remove_province_modifier = ghavaanaj_black_herd_locked
				add_permanent_province_modifier = { name = ghavaanaj_black_herd_unlocked duration = -1 }
			}
			hidden_effect = { set_country_flag = ghavaanaj_black_herd_unlocked_flag }
		}
		else = {
			custom_tooltip = ghavaanaj_herd_unlock_trigger_tooltip
		}
	}
	option = { #Palace Herd
		name = ghavaanaj.1000.e
		trigger = { NOT = { has_country_flag = ghavaanaj_palace_herd_unlocked_flag } }
		goto = 4477
		
		if = {
			limit = {
				tribal_allegiance = 60
				4477 = {
					owned_by = ROOT
					is_core = ROOT
					culture = ghavaanaj
					is_prosperous = yes
				}
			}
			4477 = {
				remove_province_modifier = ghavaanaj_palace_herd_locked
				add_permanent_province_modifier = { name = ghavaanaj_palace_herd_unlocked duration = -1 }
			}
			hidden_effect = { set_country_flag = ghavaanaj_palace_herd_unlocked_flag }
		}
		else = {
			custom_tooltip = ghavaanaj_herd_unlock_trigger_tooltip
		}
	}
	option = { #Castle Herd
		name = ghavaanaj.1000.f
		trigger = { NOT = { has_country_flag = ghavaanaj_castle_herd_unlocked_flag } }
		goto = 4483
		
		if = {
			limit = {
				tribal_allegiance = 60
				4483 = {
					owned_by = ROOT
					is_core = ROOT
					culture = ghavaanaj
					is_prosperous = yes
				}
			}
			4483 = {
				remove_province_modifier = ghavaanaj_castle_herd_locked
				add_permanent_province_modifier = { name = ghavaanaj_castle_herd_unlocked duration = -1 }
			}
			hidden_effect = { set_country_flag = ghavaanaj_castle_herd_unlocked_flag }
		}
		else = {
			custom_tooltip = ghavaanaj_herd_unlock_trigger_tooltip
		}
	}
	
	after = {
		if = {
			limit = { has_country_flag = ghavaanaj_herd_unlock_menu_open }
			country_event = { id = ghavaanaj.1000 }
		}
	}
}

###############################
#####   Herd Mechanics   ######
###############################

country_event = { #Menu
	id = ghavaanaj.2000
	title = ghavaanaj.2000.t
	desc = ghavaanaj.2000.desc
	picture = GOOD_WITH_MONARCH_eventPicture
	
	is_triggered_only = yes
	
	immediate = {
		hidden_effect = {
			set_country_flag = ghavaanaj_herd_menu_open
			if = {
				limit = { OR = { has_country_flag = ghavaanaj_herd_training_swift_flag has_country_flag = ghavaanaj_herd_training_sturdy_flag has_country_flag = ghavaanaj_herd_training_smart_flag has_country_flag = ghavaanaj_herd_training_steadfast_flag } }
				ghavaanaj_define_training_chance = yes
			}
		}
	}
	
	option = { #Nevermind
		name = flavor_castanor_go_back
		highlight = yes
		clr_country_flag = ghavaanaj_herd_menu_open
	}
	option = { #Train Harder
		name = ghavaanaj.2000.a
		trigger = { OR = { has_country_flag = ghavaanaj_herd_training_swift_flag has_country_flag = ghavaanaj_herd_training_sturdy_flag has_country_flag = ghavaanaj_herd_training_smart_flag has_country_flag = ghavaanaj_herd_training_steadfast_flag } NOT = { has_country_modifier = ghavaanaj_herd_training_harder } }
		add_mil_power = -50
		
		custom_tooltip = ghavaanaj_herd_training_train_harder_tt
		hidden_effect = {
			add_country_modifier = { name = ghavaanaj_herd_training_harder duration = 365 hidden = yes }
		}
	}
	option = { #Set Training to Swift
		name = ghavaanaj.2000.b
		trigger = { NOT = { has_country_flag = ghavaanaj_herd_training_swift_flag has_country_modifier = ghavaanaj_herd_training_physical_03 } }
		
		custom_tooltip = ghavaanaj_herd_training_set_swift_tt
		hidden_effect = {
			clr_country_flag = ghavaanaj_herd_training_sturdy_flag
			clr_country_flag = ghavaanaj_herd_training_smart_flag
			clr_country_flag = ghavaanaj_herd_training_steadfast_flag
			
			set_country_flag = ghavaanaj_herd_training_swift_flag
			
			set_variable = {
				which = ghavaanaj_herd_training_counter
				value = 0
			}
			ghavaanaj_define_training_chance = yes
		}
	}
	option = { #Set Training to Sturdy
		name = ghavaanaj.2000.c
		trigger = { NOT = { has_country_flag = ghavaanaj_herd_training_sturdy_flag has_country_modifier = ghavaanaj_herd_training_physical_13 } }
		
		custom_tooltip = ghavaanaj_herd_training_set_sturdy_tt
		hidden_effect = {
			clr_country_flag = ghavaanaj_herd_training_swift_flag
			clr_country_flag = ghavaanaj_herd_training_smart_flag
			clr_country_flag = ghavaanaj_herd_training_steadfast_flag
			
			set_country_flag = ghavaanaj_herd_training_sturdy_flag
			
			set_variable = {
				which = ghavaanaj_herd_training_counter
				value = 0
			}
			ghavaanaj_define_training_chance = yes
		}
	}
	option = { #Set Training to Smart
		name = ghavaanaj.2000.d
		trigger = { NOT = { has_country_flag = ghavaanaj_herd_training_smart_flag has_country_modifier = ghavaanaj_herd_training_mental_03 } }
		
		custom_tooltip = ghavaanaj_herd_training_set_smart_tt
		hidden_effect = {
			clr_country_flag = ghavaanaj_herd_training_swift_flag
			clr_country_flag = ghavaanaj_herd_training_sturdy_flag
			clr_country_flag = ghavaanaj_herd_training_steadfast_flag
			
			set_country_flag = ghavaanaj_herd_training_smart_flag
			
			set_variable = {
				which = ghavaanaj_herd_training_counter
				value = 0
			}
			ghavaanaj_define_training_chance = yes
		}
	}
	option = { #Set Training to Steadfast
		name = ghavaanaj.2000.e
		trigger = { NOT = { has_country_flag = ghavaanaj_herd_training_steadfast_flag has_country_modifier = ghavaanaj_herd_training_mental_13 } }
		
		custom_tooltip = ghavaanaj_herd_training_set_steadfast_tt
		hidden_effect = {
			clr_country_flag = ghavaanaj_herd_training_swift_flag
			clr_country_flag = ghavaanaj_herd_training_sturdy_flag
			clr_country_flag = ghavaanaj_herd_training_smart_flag
			
			set_country_flag = ghavaanaj_herd_training_steadfast_flag
			
			set_variable = {
				which = ghavaanaj_herd_training_counter
				value = 0
			}
			ghavaanaj_define_training_chance = yes
		}
	}
	option = { #Sell off Swift Elephants
		name = ghavaanaj.2000.f
		trigger = { OR = { has_country_modifier = ghavaanaj_herd_training_physical_03 has_country_modifier = ghavaanaj_herd_training_physical_02 } ghavaanaj_has_herd_size_2 = yes }
		
		custom_tooltip = ghavaanaj_sell_off_herd_swift_tt
		hidden_effect = {
			ghavaanaj_decrease_herd_size = yes
			if = {
				limit = { has_country_modifier = ghavaanaj_herd_training_physical_03 }
				ghavaanaj_increase_training_physical = yes
				ghavaanaj_increase_training_physical = yes
			}
			else = { ghavaanaj_increase_training_physical = yes }
		}
		add_years_of_income = 0.5
	}
	option = { #Sell off Sturdy Elephants
		name = ghavaanaj.2000.g
		trigger = { OR = { has_country_modifier = ghavaanaj_herd_training_physical_13 has_country_modifier = ghavaanaj_herd_training_physical_12 } ghavaanaj_has_herd_size_2 = yes }
		
		custom_tooltip = ghavaanaj_sell_off_herd_sturdy_tt
		hidden_effect = {
			ghavaanaj_decrease_herd_size = yes
			if = {
				limit = { has_country_modifier = ghavaanaj_herd_training_physical_13 }
				ghavaanaj_decrease_training_physical = yes
				ghavaanaj_decrease_training_physical = yes
			}
			else = { ghavaanaj_decrease_training_physical = yes }
		}
		add_years_of_income = 0.5
	}
	option = { #Sell off Smart Elephants
		name = ghavaanaj.2000.h
		trigger = { OR = { has_country_modifier = ghavaanaj_herd_training_mental_03 has_country_modifier = ghavaanaj_herd_training_mental_02 } ghavaanaj_has_herd_size_2 = yes }
		
		custom_tooltip = ghavaanaj_sell_off_herd_smart_tt
		hidden_effect = {
			ghavaanaj_decrease_herd_size = yes
			if = {
				limit = { has_country_modifier = ghavaanaj_herd_training_mental_03 }
				ghavaanaj_increase_training_mental = yes
				ghavaanaj_increase_training_mental = yes
			}
			else = { ghavaanaj_increase_training_mental = yes }
		}
		add_years_of_income = 0.5
	}
	option = { #Sell off Steadfast Elephants
		name = ghavaanaj.2000.i
		trigger = { OR = { has_country_modifier = ghavaanaj_herd_training_mental_13 has_country_modifier = ghavaanaj_herd_training_mental_12 } ghavaanaj_has_herd_size_2 = yes }
		
		custom_tooltip = ghavaanaj_sell_off_herd_steadfast_tt
		hidden_effect = {
			ghavaanaj_decrease_herd_size = yes
			if = {
				limit = { has_country_modifier = ghavaanaj_herd_training_mental_13 }
				ghavaanaj_decrease_training_mental = yes
				ghavaanaj_decrease_training_mental = yes
			}
			else = { ghavaanaj_decrease_training_mental = yes }
		}
		add_years_of_income = 0.5
	}
	
	after = {
		if = {
			limit = { has_country_flag = ghavaanaj_herd_menu_open }
			country_event = { id = ghavaanaj.2001 }
		}
	}
}

country_event = { #Menu 2
	id = ghavaanaj.2001
	title = ghavaanaj.2000.t
	desc = ghavaanaj.2000.desc
	picture = GOOD_WITH_MONARCH_eventPicture
	
	is_triggered_only = yes
	
	immediate = {
		hidden_effect = {
			set_country_flag = ghavaanaj_herd_menu_open
			if = {
				limit = { OR = { has_country_flag = ghavaanaj_herd_training_swift_flag has_country_flag = ghavaanaj_herd_training_sturdy_flag has_country_flag = ghavaanaj_herd_training_smart_flag has_country_flag = ghavaanaj_herd_training_steadfast_flag } }
				ghavaanaj_define_training_chance = yes
			}
		}
	}
	
	option = { #Nevermind
		name = flavor_castanor_go_back
		highlight = yes
		clr_country_flag = ghavaanaj_herd_menu_open
	}
	option = { #Train Harder
		name = ghavaanaj.2000.a
		trigger = { OR = { has_country_flag = ghavaanaj_herd_training_swift_flag has_country_flag = ghavaanaj_herd_training_sturdy_flag has_country_flag = ghavaanaj_herd_training_smart_flag has_country_flag = ghavaanaj_herd_training_steadfast_flag } NOT = { has_country_modifier = ghavaanaj_herd_training_harder } }
		add_mil_power = -50
		
		custom_tooltip = ghavaanaj_herd_training_train_harder_tt
		hidden_effect = {
			add_country_modifier = { name = ghavaanaj_herd_training_harder duration = 365 hidden = yes }
		}
	}
	option = { #Set Training to Swift
		name = ghavaanaj.2000.b
		trigger = { NOT = { has_country_flag = ghavaanaj_herd_training_swift_flag } }
		
		custom_tooltip = ghavaanaj_herd_training_set_swift_tt
		hidden_effect = {
			clr_country_flag = ghavaanaj_herd_training_sturdy_flag
			clr_country_flag = ghavaanaj_herd_training_smart_flag
			clr_country_flag = ghavaanaj_herd_training_steadfast_flag
			
			set_country_flag = ghavaanaj_herd_training_swift_flag
			
			set_variable = {
				which = ghavaanaj_herd_training_counter
				value = 0
			}
			ghavaanaj_define_training_chance = yes
		}
	}
	option = { #Set Training to Sturdy
		name = ghavaanaj.2000.c
		trigger = { NOT = { has_country_flag = ghavaanaj_herd_training_sturdy_flag } }
		
		custom_tooltip = ghavaanaj_herd_training_set_sturdy_tt
		hidden_effect = {
			clr_country_flag = ghavaanaj_herd_training_swift_flag
			clr_country_flag = ghavaanaj_herd_training_smart_flag
			clr_country_flag = ghavaanaj_herd_training_steadfast_flag
			
			set_country_flag = ghavaanaj_herd_training_sturdy_flag
			
			set_variable = {
				which = ghavaanaj_herd_training_counter
				value = 0
			}
			ghavaanaj_define_training_chance = yes
		}
	}
	option = { #Set Training to Smart
		name = ghavaanaj.2000.d
		trigger = { NOT = { has_country_flag = ghavaanaj_herd_training_smart_flag } }
		
		custom_tooltip = ghavaanaj_herd_training_set_smart_tt
		hidden_effect = {
			clr_country_flag = ghavaanaj_herd_training_swift_flag
			clr_country_flag = ghavaanaj_herd_training_sturdy_flag
			clr_country_flag = ghavaanaj_herd_training_steadfast_flag
			
			set_country_flag = ghavaanaj_herd_training_smart_flag
			
			set_variable = {
				which = ghavaanaj_herd_training_counter
				value = 0
			}
			ghavaanaj_define_training_chance = yes
		}
	}
	option = { #Set Training to Steadfast
		name = ghavaanaj.2000.e
		trigger = { NOT = { has_country_flag = ghavaanaj_herd_training_steadfast_flag } }
		
		custom_tooltip = ghavaanaj_herd_training_set_steadfast_tt
		hidden_effect = {
			clr_country_flag = ghavaanaj_herd_training_swift_flag
			clr_country_flag = ghavaanaj_herd_training_sturdy_flag
			clr_country_flag = ghavaanaj_herd_training_smart_flag
			
			set_country_flag = ghavaanaj_herd_training_steadfast_flag
			
			set_variable = {
				which = ghavaanaj_herd_training_counter
				value = 0
			}
			ghavaanaj_define_training_chance = yes
		}
	}
	option = { #Sell off Swift Elephants
		name = ghavaanaj.2000.f
		trigger = { OR = { has_country_modifier = ghavaanaj_herd_training_physical_03 has_country_modifier = ghavaanaj_herd_training_physical_02 } ghavaanaj_has_herd_size_2 = yes }
		
		custom_tooltip = ghavaanaj_sell_off_herd_swift_tt
		hidden_effect = {
			ghavaanaj_decrease_herd_size = yes
			if = {
				limit = { has_country_modifier = ghavaanaj_herd_training_physical_03 }
				ghavaanaj_increase_training_physical = yes
				ghavaanaj_increase_training_physical = yes
			}
			else = { ghavaanaj_increase_training_physical = yes }
		}
		add_years_of_income = 0.5
	}
	option = { #Sell off Sturdy Elephants
		name = ghavaanaj.2000.g
		trigger = { OR = { has_country_modifier = ghavaanaj_herd_training_physical_13 has_country_modifier = ghavaanaj_herd_training_physical_12 } ghavaanaj_has_herd_size_2 = yes }
		
		custom_tooltip = ghavaanaj_sell_off_herd_sturdy_tt
		hidden_effect = {
			ghavaanaj_decrease_herd_size = yes
			if = {
				limit = { has_country_modifier = ghavaanaj_herd_training_physical_13 }
				ghavaanaj_decrease_training_physical = yes
				ghavaanaj_decrease_training_physical = yes
			}
			else = { ghavaanaj_decrease_training_physical = yes }
		}
		add_years_of_income = 0.5
	}
	option = { #Sell off Smart Elephants
		name = ghavaanaj.2000.h
		trigger = { OR = { has_country_modifier = ghavaanaj_herd_training_mental_03 has_country_modifier = ghavaanaj_herd_training_mental_02 } ghavaanaj_has_herd_size_2 = yes }
		
		custom_tooltip = ghavaanaj_sell_off_herd_smart_tt
		hidden_effect = {
			ghavaanaj_decrease_herd_size = yes
			if = {
				limit = { has_country_modifier = ghavaanaj_herd_training_mental_03 }
				ghavaanaj_increase_training_mental = yes
				ghavaanaj_increase_training_mental = yes
			}
			else = { ghavaanaj_increase_training_mental = yes }
		}
		add_years_of_income = 0.5
	}
	option = { #Sell off Steadfast Elephants
		name = ghavaanaj.2000.i
		trigger = { OR = { has_country_modifier = ghavaanaj_herd_training_mental_13 has_country_modifier = ghavaanaj_herd_training_mental_12 } ghavaanaj_has_herd_size_2 = yes }
		
		custom_tooltip = ghavaanaj_sell_off_herd_steadfast_tt
		hidden_effect = {
			ghavaanaj_decrease_herd_size = yes
			if = {
				limit = { has_country_modifier = ghavaanaj_herd_training_mental_13 }
				ghavaanaj_decrease_training_mental = yes
				ghavaanaj_decrease_training_mental = yes
			}
			else = { ghavaanaj_decrease_training_mental = yes }
		}
		add_years_of_income = 0.5
	}
	
	after = {
		if = {
			limit = { has_country_flag = ghavaanaj_herd_menu_open }
			country_event = { id = ghavaanaj.2000 }
		}
	}
}

country_event = { #Training Tick
	id = ghavaanaj.2002
	title = ghavaanaj.2002.t
	desc = ghavaanaj.2002.desc
	picture = GOOD_WITH_MONARCH_eventPicture
	
	is_triggered_only = yes
	hidden = yes
	
	option = {
		if = {
			limit = { OR = { has_country_flag = ghavaanaj_herd_training_swift_flag has_country_flag = ghavaanaj_herd_training_sturdy_flag has_country_flag = ghavaanaj_herd_training_smart_flag has_country_flag = ghavaanaj_herd_training_steadfast_flag } }
			ghavaanaj_define_training_chance = yes
			ghavaanaj_roll_training_advance = yes
		}
	}
}

country_event = { #Increase Herd Size
	id = ghavaanaj.2010
	title = ghavaanaj.2010.t
	desc = ghavaanaj.2010.desc
	picture = GOOD_WITH_MONARCH_eventPicture
	
	is_triggered_only = yes
	
	option = {
		ghavaanaj_change_herd_growth = { amount = 5 }
	}
}
country_event = { #Decrease Herd Size
	id = ghavaanaj.2011
	title = ghavaanaj.2010.t
	desc = ghavaanaj.2010.desc
	picture = GOOD_WITH_MONARCH_eventPicture
	
	is_triggered_only = yes
	
	option = {
		ghavaanaj_change_herd_growth = { amount = -5 }
	}
}
country_event = { #Increase Training
	id = ghavaanaj.2012
	title = ghavaanaj.2010.t
	desc = ghavaanaj.2010.desc
	picture = GOOD_WITH_MONARCH_eventPicture
	
	is_triggered_only = yes
	
	option = {
		ghavaanaj_change_training = { amount = 100 }
	}
}